import discord
from discord.ext import commands
from time import time
import os
import json

from jikanpy import Jikan

jikan = Jikan()

class JikanAPI(commands.Cog):

    def __init__(self, client):
        self.client = client

        print(f'{os.path.basename(__file__)} loaded.')

    # Jikan API-related commands
    async def asyncFetchAnimelistWithUsername(self, username):
        return jikan.user(username=username, request='animelist', argument='completed')

    @commands.command()
    async def setup(self, ctx, MALUsername:str):
        # TODO: link discord ID with MAL user instead of using MAL user all the time
        try:
            fp = open('savedInfo/users.json', 'r')
            usersDict = json.loads(fp.read())
            fp.close()
        except FileNotFoundError:
            usersDict = {}

        try:
            usersDict[ctx.author.id]
            userAlreadySetup = True
        except KeyError:
            userAlreadySetup = False

        if userAlreadySetup == False or time() - usersDict[ctx.author.id]['setupTime'] > 86400: # If user has not been set up or wishes to refresh their list - allows manual refresh after one day
            await ctx.send(f"Setting up MAL user `{MALUsername}`...")
        
            usersDict[ctx.author.id] = await self.asyncFetchAnimelistWithUsername(MALUsername)
            usersDict[ctx.author.id]['setupTime'] = time()

            fp = open('savedInfo/users.json', 'w')
            json.dump(usersDict, fp)
            fp.close()

            await ctx.send(f"User {MALUsername} with Discord id {ctx.author.id} has been set up.")

        else:
            await ctx.send(f"User {MALUsername} with Discord id {ctx.author.id} is already set up.")

def setup(client):
    client.add_cog(JikanAPI(client))