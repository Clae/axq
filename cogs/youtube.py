# -*- coding: utf-8 -*-

import discord
from discord.ext import commands

import os
import json

import youtube_dl

# import for Youtube search - import GET request
import requests

# For percent encoding urls
import urllib

# set a global variable for volume in each guild
# TODO: save volumes on close and reload on open of bot
volumeDict = {}

class Youtube(commands.Cog):

    def importKey(self):
        fp = open('SECRETS_FILE.json', 'r')
        YTKey = json.loads(fp.read())['YTKey']
        fp.close()
        return YTKey

    def __init__(self, client):
        self.client = client
        self.YTKey = self.importKey()
        self.queueDict = {}

        print(f'{os.path.basename(__file__)} loaded.')

    # Youtube-related commands
    
    # @commands.command(pass_context=True)
    # async def play(self, ctx, url: str):
    #     server = ctx.message.server
    #     voice_client = client.voice_client_in(server)
    #     player = await voice_client.create_ytdl_player(url)
    #     players[server.id] = player
    #     player.start()

    # quiz-specific commands

    # voice-related commands
    @commands.command(aliases=['j', 'jn'])
    async def join(self, ctx):
        
        # try:
        #     await ctx.message.author.voice.channel.connect()
        # except AttributeError:
        #     await ctx.send("No voice channel error - you do not appear to be connected")
        # except CommandInvokeError: # TODO: Not sure what exception to put here
        #     await ctx.send("Already connected to your voice channel.")

        # TODO: more testing on the join
        if ctx.message.author.voice == None:
            await ctx.send("No voice channel error - you do not appear to be connected.")
        elif ctx.message.author.voice.channel.id in [voice_client.channel.id for voice_client in self.client.voice_clients]:
            await ctx.send("Already connected to your voice channel.")
        else:
            await ctx.message.author.voice.channel.connect()

    @commands.command(aliases = ['vol', 'setvolume'])
    async def volume(self, ctx, volumePercent:float):

        # TODO: implement only admin/music/DJ roles can change this value
        if volumePercent > 100 or volumePercent < 0:
            await ctx.send("Please enter a value between 0 and 100.")
        else:
            global volumeDict
            volumeDict[ctx.guild] = volumePercent
            try:
                voice_client = [voice_client for voice_client in self.client.voice_clients if voice_client.guild == ctx.guild][0]
                voice_client.source.volume = volumePercent
            except:
                pass
            
            await ctx.send(f'Volume set to {volumePercent}% in this Discord server.')

    async def YTDLPlay(self, ctx):
        
        voice_client = [voice_client for voice_client in self.client.voice_clients if voice_client.guild == ctx.guild][0]
        url, videoID, title = self.queueDict[voice_client][0]
        await ctx.send("Loading...")

        while True:

            # TODO: implement queue and multiple users playing at once.
            musicExistsAsMP3 = os.path.isfile(f"{title}-{videoID}.mp3")        
            
            
            print(f"playing {title} at {url}.")

            if not musicExistsAsMP3:

                ydl_opts = {
                    'format': 'bestaudio/best',
                    'postprocessors': [{
                        'key': 'FFmpegExtractAudio',
                        'preferredcodec': 'mp3',
                        'preferredquality': '192',
                    }],
                }
                with youtube_dl.YoutubeDL(ydl_opts) as ydl:
                    ydl.download([url])
                # for file in os.listdir("./"):
                #     if file.endswith(".mp3"):
                #         os.rename(file, 'music.mp3')
            
            # # convert to opus
            # os.system(f"ffmpeg -i {title}-{videoID}.* -f s16le -c:a libopus -b:a 128K {title}-{videoID}.opus")
            
            # os.remove(f"{title}-{videoID}.mp3")

            try:
                voice_client.volume = volumeDict[ctx.guild]
            except KeyError:
                volumeDict[ctx.guild] = 50
                voice_client.volume = 50

            # # convert to raw PCM
            # if not musicExistsAsPCM:
            #     os.system(f"ffmpeg -i {title}-{videoID}.* -f s16le -c:a pcm_s16le {title}-{videoID}.pcm")
            
            voice_client.play(discord.PCMVolumeTransformer(discord.FFmpegPCMAudio(open(f"{title}-{videoID}.mp3", 'rb')), volume=voice_client.volume))
            # TODO: this needs to be async or multithreaded, high prio
            # voice_client.play(discord.FFmpegPCMAudio(f"{title}-{videoID}.mp3"))

            self.queueDict[voice_client].pop(0)
            try:
                url, videoID, title = self.queueDict[voice_client][0]
            except IndexError:
                break

    @commands.command(aliases = ['pl', 'p', 'ply'])
    async def play(self, ctx, urlOrSearchString:str):
        
        # TODO: implement permissions checking function

        # connect if not connected
        if ctx.message.author.voice == None:
            await ctx.send("No voice channel error - you do not appear to be connected.")
        elif not ctx.message.author.voice.channel.id in [voice_client.channel.id for voice_client in self.client.voice_clients]:
            await ctx.message.author.voice.channel.connect()
        
        # youtube returns the true result when a url is searched, so always search urls and actual search terms to get metadata like video title 
        if not (urlOrSearchString[:7] == 'http://' or urlOrSearchString[:8] == 'https://'):
            urlOrSearchString = urllib.request.pathname2url(urlOrSearchString)
        
        # TODO: make get request awaitable coroutine
        search = requests.get("https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=1&q=" + urlOrSearchString + "&safeSearch=none&type=video&key=" + self.YTKey).json()

        videoID = search["items"][0]["id"]["videoId"]
        videoURL = "https://www.youtube.com/watch?v=" + videoID
        videoTitle = search["items"][0]["snippet"]["title"]

        # if currently playing, add music info to queue
        voice_client = [voice_client for voice_client in self.client.voice_clients if voice_client.guild == ctx.guild][0]

        try:
            self.queueDict[voice_client].append([videoURL, videoID, videoTitle])
        except:
            self.queueDict[voice_client] = []
            self.queueDict[voice_client].append([videoURL, videoID, videoTitle])

        await self.YTDLPlay(ctx)

        # if urlOrSearchString[:7] == 'http://' or urlOrSearchString[:8] == 'https://':
        #     # youtube-dl play from link
        #     musicName = 
        #     await self.youtubeDLPlay(ctx, urlOrSearchString, musicName)
        # else:
        #     # youtube-dl play from search
        #     searchString = urllib.request.pathname2url(urlOrSeachString)
        #     search = get("https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=1&q=" + searchString + "&safeSearch=none&type=video&key=" + self.YTKey)
        #     videoURL = "https://www.youtube.com/watch?v=" + search["items"]["id"]["videoId"]
        #     await self.youtubeDLPlay(ctx, videoURL)


    @commands.command(aliases=['l', 'lv', 'disconnect', 'dc'])
    async def leave(self, ctx):

        # TODO: more testing on the leave command, high prio
        if ctx.message.author.voice == None or not ctx.message.author.voice.channel.id in [voice_client.channel.id for voice_client in self.client.voice_clients]:

            await ctx.send("No voice channel error - I am not connected to your server.")
        else:

            await [voice_client for voice_client in self.client.voice_clients if voice_client.guild == ctx.guild][0].disconnect()



def setup(client):
    client.add_cog(Youtube(client))