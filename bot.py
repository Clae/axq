import discord
import os
from discord.ext import commands
from itertools import cycle

client = commands.Bot(command_prefix = '~')

@client.event
async def on_ready():
    print('Bot is ready!')

@client.command(aliases = ['test', 'debug', 'pg'])
async def ping(ctx):
    await ctx.send(f'pong\nmeasured latency: `{round(client.latency*1000)}` ms')

# Cog related tasks

@client.command()
async def load(ctx, cogName):
    client.load_extension(f'cogs.{cogName}')
    await ctx.send(f'Cog {cogName} loaded.')

@client.command()
async def unload(ctx, cogName):
    client.unload_extension(f'cogs.{cogName}')
    await ctx.send(f'Cog {cogName} unloaded.')

@client.command()
async def reload(ctx, cogName):
    client.unload_extension(f'cogs.{cogName}')
    client.load_extension(f'cogs.{cogName}')
    await ctx.send(f'Cog {cogName} reloaded.')

for filename in os.listdir('./cogs'):
    if filename.endswith('.py'):
        client.load_extension(f'cogs.{filename[:-3]}')

# Task to loop status

status = cycle(['Start a quiz with ~q', 'Set up with ~setup', 'Type ~help for help'])

# TODO: change status for starter commands
# @tasks.loop(seconds=5)
# async def change_status():
#     await client.change_presence(activity=discord.Game(next(status)))

# Init bot

client.run('NTkzMzk4MTY5MjcxMDA5Mjky.XRNTUA.TnSw5BTq6QFzfvJIRe1zzVtg1hs')