# AxQ

# [![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](http://makeapullrequest.com)

AxQ is a generalised quiz bot for Discord which hosts customised quizzes for members of a voice channel. It is intended to be used socially like a party game, and is inspired loosely by the web project [AMQ](https://animemusicquiz.com/), where the name derives.

The problem with a web implementation is that they require multitasking onto a web-browser and a separate login, which puts many steps between wanting to quiz and quizzing. This bot aims to fix this by directly incorporating games in Discord servers, where voice or video activity would reside regardless. 

This bot also does not attempt to be a jack-of-all trades bot unlike many closed-source alternatives. It requests **sensible** permissions upon joining and is completely open-source so you know your guild or friend's chat and voice activity stay secure.

AxQ is built upon the [discord.py API wrapper](https://github.com/Rapptz/discord.py) by Rapptz. Read about installing and deploying this bot below.

**The guiding aims are:**

- Easy to write quizzes and games with detailed documentation.<sup>1</sup>
- Easy to use by end-users in Discord.
- Fast and performant in fetching of large resources for challenge phrases.
- Free and open-source.

**Quick links:**

| Install                                                      | Usage and Creating Games                                     | Troubleshooting                                   | Contributing                     | Support us!                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------- | -------------------------------- | ------------------------------------------------------------ |
| [Installation guide](#installation). Example configuration files can be found [here](/configuration-examples). | [Usage guide](#usage), [commands](#commands), [creating you own games](#creating-games-and-quizzes). Example games can be found [here](/game-examples). | [Troubleshooting section here.](#troubleshooting) | [Contributing](CONTRIBUTING.md). | [Donate](https://liberapay.com/Clae/donate) if you love us! Stars, forks and sharing this project helps a lot too! |

<sup>1</sup> This usually depends on having an accessible API or database to create the challenge dialogues for each game. Make sure to respect rate-limits on APIs, and any proprietary terms and conditions of use for third party services.

# Installation 

AxQ can run on any system that can run Python 3, which is most operating systems and environments. Messaging performance depends largely on network latency, which can be checked after installing the bot, or by looking at the network latency in Discord voice chats, if your target system can run the normal Discord GUI client.

## Prerequisites

Make sure you have the following on the target system.

- Python 3.7 or higher
  - Some functions of the bot depend on functionality not available in earlier versions of Python.
- discord.py import module. Install this following [this guide](https://github.com/Rapptz/discord.py#installing).

Once those are installed:

1. [Begin by creating an developer application and register it as a bot with Discord](https://github.com/reactiflux/discord-irc/wiki/Creating-a-discord-bot-&-getting-a-token). This effectively creates an account for your bot and allows you to get a link for it to join servers.

2. Download the bot files from this repository with this link or by using the Download button on the right, above the latest commit. Unzip it to a location of your choice, where your bot will run out of.

3. Go to the extracted file location, and modify your SECRETS_FILE.json to contain your Discord API token key (your secret).
4. Run `python bot.py` or `python3 bot.py` to start the bot up. The bot may take a few seconds to start up, so be patient.
5. If you haven't done so already, [follow the steps](https://github.com/reactiflux/discord-irc/wiki/Creating-a-discord-bot-&-getting-a-token) to invite the bot to your server, or any servers you would like the bot to run on. Note that you must have admin permissions to invite the bot to a server. If you would only like to test the bot, just  create a new discord server and add the bot there.

See Troubleshooting below for problems with installation.

# Usage

AxQ operates off of in server text commands. Upon joining, it will only respond to the `set_channel` command which specifies a server to join where all the games and quizzes will be held. Say `set_channel "channel name"` without the quotes to assign AxQ a channel where all games will occur.

After that, refer to the following list of commands.

## Commands

| Command                                       | Purpose                                                      | Usage examples                                   |
| --------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------ |
| `set_channel`                                 | Set the primary channel of the bot, where all quizzes and games will occur. | `set_channel Quizzes`<br />`set_channel General` |
| `set_other_channels`<br />`set_other_channel` | Set other channels which quizzes and games can occur.        | `set_channel bot-spam`                           |
| `start_quiz`<br />`sq`                        | Start a quiz with all members in the voice channel of the user who called this command. | `start_quiz MAL-character`                       |
| `abort`<br />`abort_quiz`<br />`stop_quiz`    | Stop or abort the current quiz. <br />Can only be called by the initiator of the quiz, or a server admin. | `abort`                                          |
| `load_game`<br />`lg`                         | Loads a game cog, located in the game-cogs/ folder.          | `load_game MAL-character`                        |
| `unload_game`<br />`ug`                       | Unloads a currently running game cog.                        | `unload_game MAL-character`                      |
| `reload_game`<br />`rg`                       | Reloads a game cog, located in the game-cogs/ folder.        | `reload_game MAL-character`                      |



## Creating Games and Quizzes

New games and quizzes can be created with a [Cog](https://discordpy.readthedocs.io/en/latest/ext/commands/cogs.html). Create a new file/cog under game-cogs which will be the file of your new game. 

Creating new cogs requires knowledge of Python 3 and discord.py. To get started:

- Make sure you are not only comfortable with Python 3, but also with `asyncio` functions like `await` and `async def`.

- Refer to discord.py documentation above for pragramming the cog.
- See the examples included in the game-cogs folder, or even the core cogs under the cogs folder.

For debugging your cog, you can load and reload your cog. This means that you do not have to restart the bot every time you change the file contents of your cogs, only to reload the relevant files. See the commands table above.

# Troubleshooting

## Installation problems

### Trying to run the bot opens the Windows Store app

This is because you have not installed Python correctly. Try installing Python from the official website.

###  Error `ModuleNotFoundError: No module named 'discord'`

You have not installed the discord.py import correctly. [Follow this guide.](https://github.com/Rapptz/discord.py#installing)

### Error  `ModuleNotFoundError: No module named 'X'`

Some other import module is not installed. If you have used APIs requiring an import or something else that requires an import, make sure they are installed properly according to their documentation. Troubleshoot this by checking which **file** is responsible for this error.

## Usage problems

### Error `SyntaxError: invalid syntax`

If the output is

```python
File "bot.py", line 9
    async def on_ready():
        ^
SyntaxError: invalid syntax
```

Then you have run the bot with Python 2 rather than Python 3. AxQ supports only Python 3.7 and above, so install and run using the correct version of Python. If you have both versions installed, you may need to run `python3 bot.py` to run using Python 3.

For other output resulting in `SyntaxError: invalid syntax`, the above likely still applies - you have run AxQ with an unsupported Python version, since only Python 3.7+ is supported. Check your Python version with `python --version` or `python3 --version`.

### Other errors when running the bot (`python bot.py` or `python3 bot.py`)

If you have custom quizzes, then this is likely caused by incorrect configuration of the quizzes.

You may also have run AxQ with an unsupported Python version, since only Python 3.7+ is supported. Check your Python version with `python --version` or `python3 --version`.

### The bot does not respond to commands

You have either not given the bot correct permissions (read messages and etc.) or have not used the `set_channel` command yet. Check both of these are done correctly, then check that the bot is running correctly by using the `ping` command. If the bot does not respond to the `ping` command, then the bot is not running correctly in your server.